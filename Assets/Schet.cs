using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class Schet : MonoBehaviour
{
    public Text text;
    public string copyText;
    public string znak;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChisloNull(string vibor){
        switch (vibor){
            case "0": 
                text.text += "0";
                break;
            case "1": 
                text.text += "1";
                break;
            case "2": 
                text.text += "2";
                break;
            case "3": 
                text.text += "3";
                break;
            case "4": 
                text.text += "4";
                break;
            case "5": 
                text.text += "5";
                break;
            case "6": 
                text.text += "6";
                break;
            case "7": 
                text.text += "7";
                break;
            case "8": 
                text.text += "8";
                break;
            case "9": 
                text.text += "9";
                break;
            case "+":
                copyText = text.text;
                text.text = "";
                znak = "+";
                break;
            case "-":
                copyText = text.text;
                text.text = "";
                znak = "-";
                break;
            case "*":
                copyText = text.text;
                text.text = "";
                znak = "*";
                break;
            case "/":
                copyText = text.text;
                text.text = "";
                znak = "/";
                break;
            case ".":
                text.text += ",";
                break;
            case "=":
                switch (znak){
                    case "+":
                        text.text = Convert.ToString(Convert.ToDouble(copyText) + Convert.ToDouble(text.text));
                        break;
                    case "-":
                        text.text = Convert.ToString(Convert.ToDouble(copyText) - Convert.ToDouble(text.text));
                        break;
                    case "*":
                        text.text = Convert.ToString(Convert.ToDouble(copyText) * Convert.ToDouble(text.text));
                        break;
                    case "/":
                        text.text = Convert.ToString(Convert.ToDouble(copyText) / Convert.ToDouble(text.text));
                        break;
                    case "%":
                        text.text = Convert.ToString(Convert.ToDouble(copyText) % Convert.ToDouble(text.text));
                        break;
                }   
                break;
            case "clear":
                text.text = "";
                copyText = "";
                break;
            case "substr":
                text.text = text.text.Substring(0, text.text.Length - 1);
                break;
            case "coren":
                text.text = Convert.ToString(Math.Sqrt(Convert.ToDouble(text.text)));
                break;
            case "%":
                copyText = text.text;
                text.text = "";
                znak = "%";
                break;
        }
    }
}
